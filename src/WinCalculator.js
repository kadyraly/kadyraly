export const OUTCOMES = {
    ROYAL_FLUSH: 'Royal Flush',
    STRAIGHT_FLUSH: 'Straight Flush',
    FLUSH: 'Flush',
    PAIR: 'A pair',
    THREE: 'Three',
    STRAIGHT: 'Straight',
    NOTHING: 'Nothing'
};

class WinCalculator {
    constructor(cards) {
        this.cards = cards;
        this.suits = this.cards.map(card => card.suit);
        this.ranks = this.cards.map(card => card.rank);

        this.isFlush = this.suits.every(suit => suit === this.suits[0]);
    }

    isRoyalFlush() {
        return this.isFlush &&
            this.ranks.includes('10') &&
            this.ranks.includes('J') &&
            this.ranks.includes('Q') &&
            this.ranks.includes('K') &&
            this.ranks.includes('A')
    }

    isPair() {
        const ranksNumber = {};

        this.ranks.forEach(rank => {
            if (!ranksNumber[rank]) {
                ranksNumber[rank] = 1;
            } else {
                ranksNumber[rank]++;
            }
        });

        return Object.values(ranksNumber).includes(2);
    }

    isThree() {
        const ranksNumber = {};

        this.ranks.forEach(rank => {
            if (!ranksNumber[rank]) {
               ranksNumber[rank] = 1;
            } else {
                ranksNumber[rank]++;
            }
        });

        return Object.values(ranksNumber).includes(3);
    }

    isStraight() {

        if(this.cards[1].rank - this.cards[0].rank === 1 &&
            this.cards[2].rank - this.cards[1].rank === 1 &&
            this.cards[3].rank - this.cards[2].rank === 1 &&
            this.cards[4].rank - this.cards[3].rank === 1) {
            return true
        }


    }

    isStraightFlush() {
        if(this.isFlush && this.cards[1].rank - this.cards[0].rank === 1 &&
            this.cards[2].rank - this.cards[1].rank === 1 &&
            this.cards[3].rank - this.cards[2].rank === 1 &&
            this.cards[4].rank - this.cards[3].rank === 1) {
            return true
        }
    }

    getBestHand() {
        if (this.isRoyalFlush()){
            return OUTCOMES.ROYAL_FLUSH;
        } else if (this.isStraightFlush()) {
            return OUTCOMES.STRAIGHT_FLUSH;
        } else if (this.isFlush) {
            return OUTCOMES.FLUSH;
        } else if (this.isPair()) {
            return OUTCOMES.PAIR;
        } else if (this.isThree()) {
            return OUTCOMES.THREE;
        } else if (this.isStraight()) {
            return OUTCOMES.STRAIGHT;
        } else {
            return OUTCOMES.NOTHING;
        }

    }

}
 export default WinCalculator;