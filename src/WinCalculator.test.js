import WinCalculator, { OUTCOMES } from "./WinCalculator";


const royalFlush = [
    {suit: 'H', rank: 'Q'},
    {suit: 'H', rank: 'A'},
    {suit: 'H', rank: 'J'},
    {suit: 'H', rank: 'K'},
    {suit: 'H', rank: '10'},

];
const flush = [
    {suit: 'H', rank: '2'},
    {suit: 'H', rank: 'A'},
    {suit: 'H', rank: '3'},
    {suit: 'H', rank: 'K'},
    {suit: 'H', rank: '10'},

];

const pair = [
    {suit: 'H', rank: '2'},
    {suit: 'C', rank: 'A'},
    {suit: 'S', rank: '2'},
    {suit: 'D', rank: 'K'},
    {suit: 'H', rank: '10'},

];

const three = [
    {suit: 'H', rank: '10'},
    {suit: 'C', rank: 'A'},
    {suit: 'S', rank: '10'},
    {suit: 'D', rank: 'K'},
    {suit: 'H', rank: '10'},

];

const straight = [
    {suit: 'H', rank: '10'},
    {suit: 'C', rank: '9'},
    {suit: 'S', rank: '8'},
    {suit: 'D', rank: '7'},
    {suit: 'H', rank: '6'},

];

const straight = [
    {suit: 'C', rank: '10'},
    {suit: 'C', rank: '9'},
    {suit: 'C', rank: '8'},
    {suit: 'C', rank: '7'},
    {suit: 'C', rank: '6'},

];

it('should determine royal flush', () => {
    const calc = new WinCalculator(royalFlush);
    const result = calc.getBestHand();

    expect(result).toEqual(OUTCOMES.ROYAL_FLUSH);
});

it('should determine straight flush', () => {
    const calc = new WinCalculator(straightFlush);
    const result = calc.getBestHand();

    expect(result).toEqual(OUTCOMES.STRAIGHT_FLUSH);
});

it('should determine flush', () => {
    const calc = new WinCalculator(flush);
    const result = calc.getBestHand();

    expect(result).toEqual(OUTCOMES.FLUSH);
});

it('should determine a pair', () => {
    const calc = new WinCalculator(pair);
    const result = calc.getBestHand();

    expect(result).toEqual(OUTCOMES.PAIR);
});

it('should determine three', () => {
    const calc = new WinCalculator(three);
    const result = calc.getBestHand();

    expect(result).toEqual(OUTCOMES.THREE);
});

it('should determine straight', () => {
    const calc = new WinCalculator(straight);
    const result = calc.getBestHand();

    expect(result).toEqual(OUTCOMES.STRAIGHT);
});

